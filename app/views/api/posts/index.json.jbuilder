json.posts @posts.each do |post|
  json.id post.id
  json.created_at post.created_at
  json.title post.title
  json.body post.body
end

# json.bands Musician.bands.each do |band, key|
#   json.key key
#   json.name t(band, scope: 'bands')
# end

# json.partial! partial: '/api/admin/shared/pagination', locals: {
#     kind: @posts,
#     callback: 'PostStore/index'
# }