class Api::SessionsController < Api::ApiController

  def create
    user = User.find_by( email: user_params[:email])

    if user&.valid_password? user_params[:password]
      render json: user.as_json(only: [:email, :authentication_token]), status: :created
    else
      head :unauthorized
    end

  end

  def destroy

  end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end

end
