import Vue from 'vue/dist/vue.esm';

import router from './routes.js';
import store from './vuex';

import PostsForm from './components/posts/_form.vue';
Vue.component('posts-form', PostsForm);

import CSRF from './components/shared/csrf.vue';
import NavTop from './components/shared/_nav_top.vue';
Vue.component('csrf', CSRF);
Vue.component('nav-top', NavTop);


import ElementUI from 'element-ui';


// It tells vue to register ElementUI, so that we can use it
Vue.use(ElementUI);

$.ajaxSetup({
    beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
    }
});

$.ajaxPrefilter(function( options ) {
    options.url = `/api/${options.url}`;
});



const app = new Vue({
    router,
    store
}).$mount('#app')