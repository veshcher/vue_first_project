import Vue from 'vue/dist/vue.esm';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import HomeIndex from './components/home/index.vue';
import PostsIndex from './components/posts/index.vue';
import PostsNew from './components/posts/new.vue';
import PostShow from './components/posts/show.vue';
import SignUp from './components/devise/registrations/new.vue';




const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: HomeIndex, name: 'root_path' },
        { path: '/posts', component: PostsIndex, name: 'posts_path' },
        { path: '/posts/new', component: PostsNew, name: 'posts_new_path' },
        { path: '/posts/:id', component: PostShow, name: 'post_path' },
        { path: '/sign_up', component: SignUp, name: 'sign_up_path'}
    ]
});

export default router;