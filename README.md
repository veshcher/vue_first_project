Start with
```
rails new vue_first_project --webpack=vue
```

downgrade webpack-dev-server to solve exeption
(https://github.com/rails/webpacker/issues/1303)

```
yarn upgrade webpack-dev-server@^2.11.1
```

Add foreman for running multiple process at the same time from 1 command
In #Gemfile

```
gem 'foreman'
```
Create #Procfile and add

```
backend: bin/rails s -p 3000
frontend: bin/webpack-dev-server
```
Run
```
foreman start
```


To remove # from link looking like http://localhost:3000/#/posts/33
```
const router = new VueRouter({
    mode: 'history ,
    ...
```
Without this lines of code exists such problem: Can't verify CSRF token authenticity.
```
$.ajaxSetup({
    beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
    }
});
```

In #.../views/layouts/application.html.erb head:
```
<head>
  <title>VueFirstProject</title>
  <%= csrf_meta_tags %>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
  <%= stylesheet_link_tag 'application' %>
  <%= javascript_include_tag 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js' %>
</head>
```  

In #.../views/layouts/application.html.erb body:
```
<body>
    <div id='app'>
      <router-view></router-view>
    </div>
    <%= javascript_pack_tag 'application' %>
</body>
```  