Rails.application.routes.draw do


  root :to => "landing#index"
  # devise_for :users, controllers: { registrations: 'registrations' }
  namespace :api, :defaults => { :format => 'json' } do
    resources :posts
    resources :registrations, only: [:create, :destroy]
    resources :sessions, only: [:create, :destroy]
  end


  match "*path", to: "landing#index", format: false, via: :get

end
